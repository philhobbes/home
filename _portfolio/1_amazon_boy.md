---
caption: #what displays in the portfolio grid:
  title: Amazon boy
  subtitle: Boy contemplating the greatness of Rio Negro in the Amazon rain forest
  thumbnail: assets/img/portfolio/IMG_1759.jpg

#what displays when the item is clicked:
title: Amazon boy
subtitle: Boy contemplating the greatness of Rio Negro in the Amazon rain forest
image: assets/img/portfolio/IMG_1759.jpg #main image, can be a link or a file in assets/img/portfolio
alt: image alt text

---
