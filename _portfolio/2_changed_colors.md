---
caption: #what displays in the portfolio grid:
  title: Changed colors
  subtitle: Couple having a conversation, with matching colored outfits
  thumbnail: assets/img/portfolio/12190465.jpg

#what displays when the item is clicked:
title: Changed colors
subtitle: Couple having a conversation, with matching colored outfits
image: assets/img/portfolio/12190465.jpg #main image, can be a link or a file in assets/img/portfolio
alt: image alt text
---
