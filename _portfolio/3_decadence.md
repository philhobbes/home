---
caption: #what displays in the portfolio grid:
  title: Decadence
  subtitle: While some prosper, some thrive
  thumbnail: assets/img/portfolio/SAM_0019.jpg

#what displays when the item is clicked:
title: Decadence
subtitle: While some prosper, some thrive
image: assets/img/portfolio/SAM_0019.jpg #main image, can be a link or a file in assets/img/portfolio
alt: image alt text
---
